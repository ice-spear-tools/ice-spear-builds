#  Ice-Spear - Builds
![alt Ice-Spear-Builds](logo.png)

Pre-build standalone packages for Ice-Spear.

Download the newest .7z file inside the "build_win_x64" folder and extract it.

Inside the new directory run "Ice-Spear.exe" (windows) or "ice-spear-editor" (linux).

### Wiki
If you want to know how to use this tool, please check out the Wiki: <br/>
https://gitlab.com/ice-spear-tools/ice-spear/wikis/home


### License
Licensed under GNU GPLv3.  
For more information see the LICENSE file in the project's root directory